// LOCAL STORAGE
if (sessionStorage.length === 0) {
  sessionStorage.setItem("Score", 0);
  sessionStorage.setItem("Score_history", JSON.stringify([]));
  sessionStorage.setItem("Overall_accuracy", 0);
}
let scoreAsString = sessionStorage.getItem("Score");
let scoreAsNumber = JSON.parse(scoreAsString);
let scoreHistoryAsString = sessionStorage.getItem("Score_history");
let scoreHistoryAsArray = JSON.parse(scoreHistoryAsString);
let overallAccuracyAsString = sessionStorage.getItem("Overall_accuracy");
let overallAccuracyAsNumber = JSON.parse(overallAccuracyAsString);
let last5AccuracyAsString;
let last5AccuracyAsNumber;
console.dir(sessionStorage);

// Local Storage -- functions
function setLast5Accuracy() {
  let last5 = scoreHistoryAsArray.slice(-5);
  let total = last5.reduce((a, b) => a + b, 0);
  let accuracy = (total / 5) * 100;
  sessionStorage.setItem("Last_5_accuracy", accuracy);

  last5AccuracyAsString = sessionStorage.getItem("Last_5_accuracy");
  last5AccuracyAsNumber = JSON.parse(last5AccuracyAsString);
  last5Accuracy.innerText = `Your last-5 accuracy is ${last5AccuracyAsString}%`;
}

function setOverallAccuracy() {
  let totalScore = scoreHistoryAsArray.reduce((a, b) => a + b, 0);
  let totalLength = scoreHistoryAsArray.length;
  let accuracy = Math.round((totalScore / totalLength) * 100);
  sessionStorage.setItem("Overall_accuracy", accuracy);
  overallAccuracyAsString = sessionStorage.getItem("Overall_accuracy");
  overallAccuracy.innerText = `Your overall accuracy is ${overallAccuracyAsString}%`;
}

function messageGenerator() {
  if (last5AccuracyAsNumber > overallAccuracyAsNumber) {
    encouragingMessage.innerText =
      "When your last-5 accuracy is better than your overall accuracy, you're improving!  👏👏👏";
  } else return;
}

// OTHER DECLARATIONS
const congratsMessage = document.getElementById("success");
const failureMessage = document.getElementById("failure");
const nextPage = document.getElementById("next-page");
const answerForm = document.getElementById("answer-div");
const playCBtn = document.getElementById("play-c-btn");
const playNoteBtn = document.getElementById("play-note-btn");

let scoreDisplay = document.getElementById("score-display");
let overallAccuracy = document.getElementById("overall-accuracy");
let last5Accuracy = document.getElementById("last-5-accuracy");
let encouragingMessage = document.getElementById("encouraging-message");
let note;
let selectedNote;
scoreDisplay.innerText = headerText();

function headerText() {
  if (scoreHistoryAsArray === null) {
    return `Welcome to the Ear Trainer! Play Note to start.`;
  } else {
    return `Score: ${scoreAsString}/${scoreHistoryAsArray.length}`;
  }
}

// RANDOM </div> SELECTION
(function assignRandomNote() {
  const notes = ["a", "b", "c", "d", "e", "f", "g", "high-c"];
  const ranIndex = (() => _.random(0, 7))();
  selectedNote = notes[ranIndex];

  const audioToSet = document.getElementById("random-note");
  audioToSet.innerHTML = `<source id="note-to-change" src="/${selectedNote}.mp3" type="audio/mpeg">`;
})();

// GENERAL FUNCTIONS

function playMiddleC() {
  document.getElementById("audioContainer-c").play();
}

function playRandomNote() {
  note = document.getElementById("random-note");
  note.play();
}

function resetScore() {
  sessionStorage.setItem("Score", 0);
  sessionStorage.setItem("Score_history", JSON.stringify([]));
  sessionStorage.setItem("Overall_accuracy", 0);
  sessionStorage.Last_5_accuracy ? sessionStorage.removeItem("Last_5_accuracy") : null;
}

function refreshPage() {
  location.reload();
}

// check answer
function checkAnswer() {
  let userInput;

  cRadioAns ? (userInput = "c") : null;
  dRadioAns ? (userInput = "d") : null;
  eRadioAns ? (userInput = "e") : null;
  fRadioAns ? (userInput = "f") : null;
  gRadioAns ? (userInput = "g") : null;
  aRadioAns ? (userInput = "a") : null;
  bRadioAns ? (userInput = "b") : null;
  highCRadioAns ? (userInput = "high-c") : null;

  // Side-effects of submitting answer
  if (userInput === selectedNote) {
    congratsMessage.setAttribute("class", "visible");

    scoreAsNumber++;
    sessionStorage.setItem("Score", scoreAsNumber);

    scoreHistoryAsArray ? scoreHistoryAsArray.push(1) : null;
    sessionStorage.setItem("Score_history", JSON.stringify(scoreHistoryAsArray));

    scoreAsString = sessionStorage.getItem("Score");
    scoreDisplay.innerText = `Total score: ${scoreAsString} out of ${scoreHistoryAsArray.length}`;
  } else {
    failureMessage.setAttribute("class", "visible");
    failureMessage.innerText = `Sorry, the note played was ${selectedNote.toUpperCase()}.`;

    scoreHistoryAsArray ? scoreHistoryAsArray.push(0) : null;
    sessionStorage.setItem("Score_history", JSON.stringify(scoreHistoryAsArray));
  }
  answerForm.setAttribute("class", "invisible");
  playCBtn.setAttribute("class", "invisible");
  playNoteBtn.setAttribute("class", "invisible");
  nextPage.setAttribute("class", "visible");
  scoreHistoryAsArray ? (scoreHistoryAsArray.length > 4 ? setLast5Accuracy() : null) : null;
  setOverallAccuracy();
  messageGenerator();
  console.dir("updated sessionStorage: ", sessionStorage);
}

// event listener to Check Answer
document.getElementById("check-answer").addEventListener("click", function (event) {
  event.preventDefault();
  checkAnswer();
});
